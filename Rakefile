require 'erb'
require 'pp'

ENV['VAGRANT_BOX'] ||= 'vagrant.box'
ENV['DISK_SIZE'] ||= '102400'

vagrant_box = ENV['VAGRANT_BOX']
new_size = ENV['DISK_SIZE']

vagrant_orig_disk = "#{Dir.home}/.vagrant.d/boxes/#{vagrant_box}/virtualbox/box-disk1.vmdk"
vagrant_tmp_vdi = "/var/tmp/#{vagrant_box}_box-disk1-NEW-TMP.vdi"
vagrant_new_vmdk = "/var/tmp/#{vagrant_box}_box-disk1-RESIZED.vmdk"

vbox_manage_cmd = `which VBoxManage`.chomp
vagrant_cmd = `which vagrant`.chomp

def log(msg)
  puts ">>> %s" % msg
end

def ask(prompt, env, default="")
    return ENV[env] if ENV.include?(env)

    if default
      print "#{prompt} (#{default}): "
    else
      print "#{prompt}: "
    end

    resp = STDIN.gets.chomp

    resp.empty? ? default : resp
end

desc "Clone given box's disk as vdi, so it can be resized"
task :clone_and_resize do
    if ! File.exists? vagrant_tmp_vdi
        sh "#{vbox_manage_cmd} clonehd #{vagrant_orig_disk} #{vagrant_tmp_vdi} --format vdi"
        sh "#{vbox_manage_cmd} modifyhd #{vagrant_tmp_vdi} --resize #{new_size}"
    end
    if ! File.exists? vagrant_new_vmdk
        sh "#{vbox_manage_cmd} clonehd #{vagrant_tmp_vdi} #{vagrant_new_vmdk} --format vmdk"
    end
end

desc "Vagrant up with base box, wait for it to complete"
task :vagrant_up => [ "clone_and_resize" ] do
    sh "#{vagrant_cmd} up"
end

desc "All LVM operations"
task :disk_operations => [ "vagrant_up" ] do

    vg_name = `#{vagrant_cmd} ssh -c \"sudo pvs | grep sdb | awk \'{print \\$2}\'\"`.chomp
    pv_name = `#{vagrant_cmd} ssh -c \"sudo pvdisplay | grep sdb | awk \'{print \\$3}\'\"`.chomp
    lv_name = `#{vagrant_cmd} ssh -c \"sudo lvdisplay #{vg_name} | grep root | awk \'{print \\$3}\'\"`.chomp

    disk_dev = '/dev/sdb'
    fdisk_out = `#{vagrant_cmd} ssh -c \"sudo fdisk -l #{disk_dev}"`
    fdisk_array = []
    #fdisk_out.each_line { |line| fdisk_array << line if line =~ /^\/dev\/sdb/ }
    fdisk_out.each_line { |line| fdisk_array << line if line =~ /^#{disk_dev}/ }

    extended_partition = false
    fdisk_array.each {|e|
        if e =~ /Extended$/
            extended_partition = true
        end
    }

    disk_operations_template = ERB.new <<-EOF
#!/bin/bash

vgchange -a n #{vg_name}
<% if extended_partition == true %>
fdisk #{disk_dev} <<-EOFDISK
d
2
n
e
2


n
l


t
5
8e
w
EOFDISK
<% else %>
fdisk #{disk_dev} <<-EOFDISK
d
2
n
p
2


t
2
8e
w
EOFDISK
<% end %>

partprobe -s
pvresize #{pv_name}
lvextend -l 100%FREE #{lv_name}
e2fsck -y -f #{lv_name}
resize2fs #{lv_name}

exit $?
    EOF
    buffer = disk_operations_template.result(binding)
    target_script = "#{Dir.pwd}/fdisk-and-extend.sh"

    File.open("#{target_script}", 'w') do |f|
        f.write buffer
    end

    File.chmod(0777, target_script)

    sh "#{vagrant_cmd} ssh -c 'sudo /vagrant/fdisk-and-extend.sh'"

end

